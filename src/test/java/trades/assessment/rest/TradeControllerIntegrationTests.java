package trades.assessment.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import trades.assessment.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Transactional
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests {
	
	private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);
	
	@Autowired
    private TestRestTemplate restTemplate;
 
    @Test
    public void getTrade_returnsTrade() {
        restTemplate.postForEntity("/trades",
                                   new Trade(-1, "Pears", 123.23, 300), Trade.class);

        ResponseEntity<List<Trade>> getAllResponse = restTemplate.exchange(
                                "/trades",
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trade>>(){});

        logger.info("getAllTrade response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getStock().equals("Pears"));
        assertEquals(getAllResponse.getBody().get(0).getPrice(), 123.23, 0.0001);
        assertEquals(getAllResponse.getBody().get(0).getVolume(), 300);
    }
}
