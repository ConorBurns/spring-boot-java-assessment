package trades.assessment.model;

import static org.junit.Assert.*;

import org.junit.Test;

import trades.assessment.model.Trade;

public class TradeTests {

	private int id = 3;
	private String stock = "Apples";
	private double price = 1.2;
	private int volume = 100;
	
	@Test
	public void test_trade_constructor() {
		Trade newTrade = new Trade(id,stock,price,volume);
		
		assertEquals(id, newTrade.getId());
		assertEquals(stock, newTrade.getStock());
		assertEquals(price, newTrade.getPrice(), 1.2);
		assertEquals(volume, newTrade.getVolume());
	}

	@Test
	public void test_trade_toString() {
		String testString = new Trade(id,stock,price,volume).toString();
		
		  assertTrue(testString.contains((new Integer(id)).toString()));
	      assertTrue(testString.contains(stock));
	      assertTrue(testString.contains(String.valueOf(price)));
	      assertTrue(testString.contains(String.valueOf(volume)));
	}
}
