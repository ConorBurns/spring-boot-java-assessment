package trades.assessment.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import trades.assessment.model.Trade;
import trades.assessment.service.TradeService;

@RestController
@RequestMapping("/trades")
public class TradeController {

	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

	@Autowired
	private TradeService tradeService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Trade> findAll() {
		LOG.debug("Finding all Trades -  findall() was called");
		return tradeService.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Trade findById(@PathVariable int id) {
		LOG.debug("Finding by ID - findById() was called");
		return tradeService.findById(id);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trade> create(@RequestBody Trade trade) {
		LOG.debug("creating a Trade - create() was called");
		return new ResponseEntity<Trade>(tradeService.create(trade), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public @ResponseBody void deleteById(@PathVariable int id) {
		LOG.debug("deleting an trade - deleteById() was called.");
		tradeService.deleteById(id);
	}

}
