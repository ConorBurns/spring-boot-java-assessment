package trades.assessment.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import trades.assessment.exceptions.TradeNotFoundException;
import trades.assessment.model.Trade;

@Component
@Profile("inmem-dao")
public class InMemTradeDao implements TradeDao{

	private static AtomicInteger idGenerator = new AtomicInteger();
	private Map<Integer, Trade> allTrades = new HashMap<Integer, Trade>();
	
	@Override
	public List<Trade> findAll() {
		return new ArrayList<Trade>(allTrades.values());
	}

	@Override
	public Trade findById(int id) {
		Trade trade = allTrades.get(id);
        if (trade == null) {
            throw new TradeNotFoundException("The Trade has not been found.");
        }
        return trade;
	}

	@Override
	public Trade create(Trade trade) {
		trade.setId(idGenerator.addAndGet(1));
        allTrades.put(trade.getId(), trade);
        return trade;
	}

	@Override
	public void deleteById(int id) {
		Trade trade = allTrades.remove(id);
        if (trade == null) {
            throw new TradeNotFoundException("Trade could not be deleted");
        }
		
	}

}
