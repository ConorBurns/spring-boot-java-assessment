package trades.assessment.dao;

import java.util.List;
import trades.assessment.model.Trade;

public interface TradeDao {
	
	List<Trade> findAll();

    Trade findById(int id);

    Trade create(Trade trade);

    void deleteById(int id);

}
