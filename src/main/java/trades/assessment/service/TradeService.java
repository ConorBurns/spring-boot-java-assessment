package trades.assessment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import trades.assessment.dao.TradeDao;
import trades.assessment.model.Trade;

@Component
public class TradeService {
	
	@Autowired
	private TradeDao tradeDao;
	
	public List<Trade> findAll(){
		return tradeDao.findAll();
	};

	public Trade findById(int id) {
		return tradeDao.findById(id);
	};

	public Trade create(Trade trade) {
		return tradeDao.create(trade);
	};

	public void deleteById(int id) {
		tradeDao.deleteById(id);
	};

}
